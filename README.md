# README #

Hello, and welcome to **BlockProj**.

### What is this repository for? ###

BlockProj is a project which aimed to find solutions (written by me, as opposed to nabbed from someone else) for the problems of finding the areas of the largest rectangle in a grid and the largest shape in a grid. Classes for Grids and Blocks (the items that fit in Grids) are provided and have some basic functionality.

## Version 0.1 ##
Initial version. No changes as this is the first one. Interfaces for things will be coming shortly.

### Future projects ###
* outputting shape findings to Swing window
* replacing the System.err.println stuff with proper logging

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact