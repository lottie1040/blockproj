package com.azuregem.blockproj;

public class ShapeScan {
	/* Scans to find area of largest shape on a grid.
	 * This only works on grids 3x3 or larger.
	 * Charlotte Lunn, 11 June 2016 */
	
	private Grid toCheck;
	private int[][] connections;
	private int gridW, gridH;
	private int currentShape;
	private int[] countOfShapes;
	
	private final int MIN_DIMENSIONS = 3;			// minimum dimensions of grid (width and height)
	
	public ShapeScan() {
		this.toCheck = null;
	}
	
	public ShapeScan (Grid toCheck) {
		feedGrid(toCheck);
	}
	
	public void feedGrid (Grid toCheck) {
		// This is the same one as from GridCheck
		this.toCheck = toCheck;
		
		// get the width and height of the grid from the grid itself
		this.gridW = this.toCheck.getWidth();
		this.gridH = this.toCheck.getHeight();
		
		if(this.gridW < this.MIN_DIMENSIONS || this.gridH < this.MIN_DIMENSIONS) {
			System.err.println("Sorry, the grid is too small.");		// this will be replaced with a log entry
			this.toCheck = null;									// get rid of grid
		}
		
		this.connections = new int[this.gridW][this.gridH];
	}
	
	public int scanIt () {
		verticalScan();						// first pass
		horizontalScan();
		
		for (int passes = 0; passes < 3; passes++) {
			this.countOfShapes = findCount();
			verticalRescanBW();					// second, third & fourth pass
			this.countOfShapes = findCount();
			horizontalRescan();
		}
		
		int result = getAreaOfLargest();
		return result;
	}
	
	public void connectionsPrint() {
		String introPhrase = "Map of shapes:\n";
		System.out.print(introPhrase);
		
		for (int i = 0; i < this.gridH; i++) {
			for (int j = 0; j < this.gridW; j++) {
				String output = "";
				if (currentShape >= 100 && connections[j][i] < 100) {
					output += " ";
				}
				
				if (connections[j][i] < 10) {
					output += " ";				// if the no. is less than 10, space it out
				}
				
				output += connections[j][i];
				
				if (j != this.gridW - 1) {		// is it the last one in the row?
					output += ", ";				// if not, put in a comma and space
				}

				System.out.print(output);
			}
			System.out.print("\n");
		}
	}
	
	private void verticalScan () {
		final int ySTART = 1;			// starting at 1 rather than 0. Reason will become apparent
		int currentShape = 0;
		
		for(int x = 0; x < this.gridW; x++) {
			currentShape++;
			for (int y = ySTART; y < this.gridH - 1; y++) {
				if(this.toCheck.gridItem(x,y).isActive()){
					if(this.toCheck.gridItem(x, y-1).isActive()){		// look up...
						connections[x][y] = currentShape;
						connections[x][y-1] = currentShape;
					}
					if(this.toCheck.gridItem(x, y+1).isActive()){		// look down...
						connections[x][y] = currentShape;
						connections[x][y+1] = currentShape;
					}
				} else {
					currentShape++;
				}
			} // end for y
		} // end for x
		this.currentShape = currentShape;
	}
	
	private void horizontalScan () {
		final int xSTART = 1;			// as before
		int currentShape = this.currentShape + 1;
		
		for (int y = 0; y < this.gridH; y++) {
			for (int x = xSTART; x < this.gridW - 1; x++) {
				if(this.toCheck.gridItem(x, y).isActive() && connections[x][y] == 0) {
					connections[x][y] = currentShape;
				}
				if (connections[x][y] > 0) {
					if (this.toCheck.gridItem(x-1, y).isActive()) {
						connections[x-1][y] = connections[x][y];
					}
					
					if (this.toCheck.gridItem(x+1, y).isActive()) {
						connections[x+1][y] = connections[x][y];
					}
					
				} // end if
				currentShape++;
			} // end for x
		} // end for y
		this.currentShape = currentShape;		// this now marks the largest shape number
	}
	
	private void verticalRescanBW() {
		// The vertical backwards rescan merges some unmerged shapes.
		final int ySTART = 1;
		
		for(int x = 0; x < this.gridW; x++) {
			for (int y = this.gridH - 1; y >= ySTART; y-=1) {
				if(this.connections[x][y] > 0 && this.connections[x][y-1] > 0){
					 if(this.countOfShapes[this.connections[x][y]] > this.countOfShapes[this.connections[x][y-1]]) {
						connections[x][y-1] = connections[x][y];
					} else {
						connections[x][y] = connections [x][y-1];
					}
				}
				
				if(this.connections[x][y] > 0 && y != this.gridH - 1 && this.connections[x][y+1] > 0){
					if(this.countOfShapes[this.connections[x][y]] > this.countOfShapes[this.connections[x][y+1]]){
						connections[x][y+1] = connections[x][y];
					} else {
						connections[x][y] = connections[x][y+1];
					}
				}
			} // end for y
		} // end for x
	} // end verticalBackwardsRescan method

	
	private void horizontalRescan() {
		/* Finally, the horizontal rescan merges some *more* unmerged shapes.
		 * It goes from right to left this time.
		 */
		
		final int xSTART = 1;
		
		for(int y = 0; y < this.gridH; y++) {
			for (int x = this.gridW - 1; x >= xSTART; x-=1) {
				if(this.connections[x][y] > 0 && this.connections[x-1][y] > 0){
					if (this.countOfShapes[this.connections[x-1][y]] > this.countOfShapes[this.connections[x][y]]) {	
						this.connections[x][y] = this.connections[x-1][y];
					} else {
						this.connections[x-1][y] = this.connections[x][y];
					}
				}
				
				if(this.connections[x][y] > 0 && x != this.gridW - 1 && this.connections[x+1][y] > 0){
					if (this.countOfShapes[this.connections[x+1][y]] > this.countOfShapes[this.connections[x][y]]) {	
						this.connections[x][y] = this.connections[x+1][y];
					} else {
						this.connections[x+1][y] = this.connections[x][y];
					}
				}
				
			} // end for y
		} // end for x
	} // end horizontalRescan method
	
	private int[] findCount() {
		int maxShape = this.currentShape;
		int[] countOfEach = new int[maxShape + 1];
		
		for (int x = 0; x < this.gridW; x++) {
			for (int y = 0; y < this.gridH; y++) {
				if (connections[x][y] != 0) {
					countOfEach[connections[x][y]]++;
				}
			}
		}
		return countOfEach;
	}
	
	private int getAreaOfLargest() {
		int largestArea = 0;
		
		for(int shapes = 1; shapes <= this.currentShape; shapes++) {
			int areaOfShape = 0;		
			for(int x = 0; x < this.gridW; x++) {
				for (int y = 0; y < this.gridH; y++) {
					if (this.connections[x][y] == shapes) {
						areaOfShape++;
					}
				}
			}
			if (areaOfShape > largestArea) {
				largestArea = areaOfShape;
			}
		}
		
		return largestArea;
	}
}
