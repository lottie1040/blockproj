package com.azuregem.blockproj;

import java.io.IOException;

public class Test {

	// This is a series of tests, in order to test the grid.
	// It's not a formal series of tests by any means and doesn't test much of the Block and Grid
	// functionality alone.
	
	public static void main(String[] args) {
		
		GridLoader loader = new GridLoader();
		
		try {
			Grid testGridTwo = loader.loadFile("D:\\Dropbox\\Code\\Java\\BlockProj\\shapes\\chainlink.blk");
			
			ShapeScan scannerTwo = new ShapeScan(testGridTwo);
			int largestShapeArea = scannerTwo.scanIt();
			String largestShapeOutput = "Largest shape area: " + largestShapeArea + " blocks.";
			System.out.println(largestShapeOutput);
			scannerTwo.connectionsPrint();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		/* Previous testing...
		 * I'm aware activate() returns an int as a status, but I'd forgotten when I was doing this testing.
		 * If I end up using it again, I'll add in an int to retrieve the activation status of each block.

		Grid testing = new Grid(6,8);
		
		testing.activate(1, 1);
		testing.activate(1, 2);
		testing.activate(1, 3);
		testing.activate(2, 1);
		testing.activate(3, 1);
		testing.activate(3, 2);
		testing.activate(3, 3);
		testing.activate(3, 4);
		testing.activate(5, 4);
		testing.activate(5, 5);
		testing.activate(5, 6);
		testing.activate(1, 7);
		testing.activate(2, 7);
		testing.activate(3, 7);
		
		GridCheck checker = new GridCheck(testing);
		int result = checker.scanGrid();
		// System.out.println(result);
		// System.out.println(testing.toString());
		
		ShapeScan scanner = new ShapeScan(testing);
		result = scanner.scanIt();
		
		System.out.println("Largest area: " + result + " blocks in size.");
		*/
		
	}

}
