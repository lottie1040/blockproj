/**
 * 
 */
/**
 * @author Charlotte Lunn
 * @version 0.1.4
 */
package com.azuregem.blockproj;

/* This probably isn't standard,
 * but figured I should add a version history somewhere.
 * I'll make it javadoc-compliant later.
 * 
 * 0.1.4: Major bug-fixes.
 * Circles and shapes with crosses would not always be identified as contiguous prior to this update.
 * ShapeScan.horizontalRescan() now goes from right to left.
 * verticalRescan() replaced with verticalRescanBW()
 * Removed unnecessary code from horizontalScan() - unnecessary sub-scan.
 * 
 * Also some other minor changes to make ShapeScan.connectionsPrint()
 * a bit more user-friendly.
 */
