package com.azuregem.blockproj;

public class GridCheck {
	
	/* GridCheck class, part of the BlockProj
	 * by Charlotte Lunn, 11 June 2016
	 * 
	 * www.azuregem.com
	 * 
	 * Not sure what the license should be, but I'll think of it later.
	 * 
	 * This class checks for the largest rectangle in any given Grid and gives its area.
	 * There is a more complicated (but maybe messier) class, ShapeScan, that finds the largest shape
	 * in a Grid and gives its area.
	 * 
	 * Many thanks to Tushar Roy for his excellent video on this subject. Please see his YouTube channel:
	 * https://www.youtube.com/user/tusharroy2525
	 */
	
	private Grid toCheck;
	private int[] histogram;		// new int[] will follow later...
	private int gridW, gridH;
	
	public GridCheck () {
		System.err.println("No grid specified!");
		this.toCheck = null;
	}
	
	public GridCheck (Grid toCheck) {
		feedGrid(toCheck);
	}
	
	public void feedGrid (Grid toCheck) {
		this.toCheck = toCheck;
		
		// get the width and height of the grid from the grid itself
		this.gridW = this.toCheck.getWidth();
		this.gridH = this.toCheck.getHeight();
		
		this.histogram = new int[gridW];		// see? I told you ;)
	}
	
	public int scanGrid() {
		int biggestRectangle = 0;			// variable to store area of largest rectangle
		
		int histCurrHeight = 0;				// variable for the current height of histogram columns
											// (if the column is higher than 1)
		int currRectangle = 0;				// temp variable for area of rectangle being found
		
		// Outer loop, going from row to row.
		for (int yloop = 0; yloop < this.gridH; yloop++) {
			// Inner loop, checking individual blocks.
			for (int xloop = 0; xloop < this.gridW; xloop++) {
				int currentItem;
				if(this.toCheck.gridItem(xloop, yloop).isActive()) {
					currentItem = 1;
				} else {
					currentItem = 0;
				}
				if (currentItem == 1) {
					this.histogram[xloop] += currentItem;			// add the item to the histogram
				} else {
					this.histogram[xloop] = 0;						// or if it's blank, reset the column
				}
			}
			
			// Reset variables (if necessary)
			currRectangle = 0;
			
			// Once the individual block check is done, check for rectangles.
			for (int xloop = 0; xloop < this.gridW; xloop++) {
				if (this.histogram[xloop] > 0) {
					histCurrHeight = this.histogram[xloop];
					currRectangle = histCurrHeight;
					// we need another loop, to check the items along from the one being checked
					for (int loopthree = xloop + 1; loopthree < this.gridW; loopthree++) {
						if (this.histogram[loopthree] >= histCurrHeight) {
							currRectangle += histCurrHeight;
						} else {
							// if it's the end of the rectangle, break out of loop three
							break;
						}
					} // end loop three
					
					if (currRectangle > biggestRectangle) {
						/* so if the rectangle just found is bigger than the biggest so far,
						 * replace the biggestRectangle area with this new, bigger one. */
						biggestRectangle = currRectangle;
					}
				} // end if
			} // end inner loop
			
		} // end outer loop
		
		return biggestRectangle;		// this bit should be self-explanatory
	}
	
	public String toString() {
		String output = "This is the GridCheck class.\n";
		output += "It checks grids for rectangles of activated blocks.";
		return output;
	}
	
}
