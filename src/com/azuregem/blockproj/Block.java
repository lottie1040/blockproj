package com.azuregem.blockproj;

public class Block {
	/* Block class, for the BlockProj
	 * Charlotte Lunn, 10 June 2016
	 * cnlunn@gmail.com
	 * 
	 * A Block is an object which is designed to be part of a Grid, but it doesn't have to be.
	 * Not sure why you would want one on its own, but it's doable.
	 */
	
	private boolean active;
	private boolean read;
	private boolean ownerSet = false;
	private int xLoc, yLoc;

	private Grid owner;
	
	public Block() {
		this(-1,-1,false);
	}
	
	public Block (boolean active) {
		this(-1,-1,active);
	}
	
	public Block (int xLoc, int yLoc, boolean active) {
		this.xLoc = xLoc;
		this.yLoc = yLoc;
		setActive(active);
	}

	// getters and setters below
	
	public void setOwner(Grid owner) {
		this.owner = owner;
		ownerSet = true;
	}
	
	public Grid getOwner() {
		if (isOwnerSet()) {
			return this.owner;
		} else {
			return null;
		}
	}
	
	public boolean isOwnerSet() {
		return this.ownerSet;
	}

	public boolean isActive() {
		return this.active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public boolean isRead() {
		return this.read;
	}

	public void setRead(boolean read) {
		this.read = read;
	}
	
	// and the toString() method
	
	public int getxLoc() {
		return this.xLoc;
	}

	public void setxLoc(int xLoc) {
		this.xLoc = xLoc;
	}

	public int getyLoc() {
		return this.yLoc;
	}

	public void setyLoc(int yLoc) {
		this.yLoc = yLoc;
	}

	public String toString() {
		
		String result = "This is a Block. It is currently ";
		boolean activity = isActive();
		if(activity) {
			result += "active.\n";
		} else {
			result += "inactive.\n";
		}
		
		if (xLoc == -1) {
			result += "Its location is not defined.\n";
		} else {
			result += "It is at location " + xLoc + "," + yLoc + ".\n";
		}
		
		if(ownerSet) {
			int gridID = getOwner().getGridID();
			result += "It is a member of the grid with ID " + gridID + ".";
		} else {
			result += "It is not a member of any grid.";
		}
		
		return result;
	}
	
}
