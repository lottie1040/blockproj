package com.azuregem.blockproj;

public class Grid {
	
	/* Grid class
	 * Part of the BlockProj (see package-info)
	 * Designed to work with the Block class from the same package
	 * 
	 * Charlotte Lunn, 10 June 2016
	 */
	
	private int width, height;
	private final static int DEF_WIDTH = 2;
	private final static int DEF_HEIGHT = 2;
	
	private int gridID;
	private static int gridIDmax = 0;
	
	public final int ACTIVATE_FAIL = -1, ALREADY_ACTIVE = 0, ACTIVATE_OK = 1;
	
	private Block[][] theBlock;
	
	public Grid () {
		this(DEF_WIDTH, DEF_HEIGHT);
	}
	
	public Grid(int width, int height) {
		theBlock = new Block[width] [height];
		setWidth(width);
		setHeight(height);
		
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				Block blank = new Block(x,y,false);
				blank.setOwner(this);
				theBlock [x] [y] = blank;
			}
		}
		
		gridIDmax++;				// increase the current highest grid ID by one
		setGridID(gridIDmax);		// ...and set the grid's ID to that.
		
	}
	
	public Block gridItem (int x, int y) {
		try {
			return theBlock [x] [y];
		} catch (IndexOutOfBoundsException e) {
			// Is a grid item outside the boundaries of the grid trying to be accessed?
			String error = "Out of bounds! (Location " + x + "," + y + ")\n";
			error += "Dummy grid item returned.";
			System.err.println(error);
			Block dummy = new Block();
			return dummy;
		}
		
	}
	
	public int activate (int x, int y) {
		try {
			if (theBlock [x] [y].isActive()) {
				return this.ALREADY_ACTIVE;
			} else {
				theBlock [x] [y].setActive(true);
				return this.ACTIVATE_OK;
			}
		} catch (IndexOutOfBoundsException e) {
		    System.err.println("IndexOutOfBoundsException: " + e.getMessage());
		    return this.ACTIVATE_FAIL;
		}
	}

	public int getWidth() {
		return this.width;
	}

	private void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return this.height;
	}

	private void setHeight(int height) {
		this.height = height;
	}

	public int getGridID() {
		return this.gridID;
	}

	private void setGridID(int gridID) {
		this.gridID = gridID;
	}
	
	public String toString() {
		String output = "This is a grid with dimensions ";
		output += width + " by " + height + ".";
		return output;
	}
	
}
