package com.azuregem.blockproj;

import java.io.IOException;
import java.io.FileReader;
import java.io.BufferedReader;

public class GridLoader {
	/* GridLoader class
	 * Loads in a grid from a file with a specified format.
	 * 
	 * Charlotte Lunn, 11 June 2016
	 */
	
	private int numberOfLines;
	
	public GridLoader () {
		// default constructor
	}
	
	public Grid loadFile(String filePath) throws IOException {
		this.numberOfLines = readLines(filePath);
		
		String[] data = new String[this.numberOfLines];
			
		FileReader fr = new FileReader(filePath);
		BufferedReader br = new BufferedReader(fr);
			
		for(int line=0; line < this.numberOfLines; line++) {
			data[line] = br.readLine();
		}
			
		br.close();
		
		Grid result = stringArrToGrid(data);
		return result;
	}
	
	private Grid stringArrToGrid (String[] array) {
		int width = array[0].length();
		
		Grid toReturn = new Grid(width, this.numberOfLines);
		
		for(int y=0; y < this.numberOfLines; y++) {
			for(int x=0; x < width; x++) {
				
				try {
					if(array[y].charAt(x) == '.') {
						toReturn.gridItem(x, y).setActive(true);
						toReturn.gridItem(x, y).setOwner(toReturn);
					}
				} catch (StringIndexOutOfBoundsException e) {
					break;
				}
			}
		}
		
		return toReturn;
	}
	
	private int readLines(String filePath) throws IOException {
		FileReader rfr = new FileReader(filePath);
		BufferedReader rbr = new BufferedReader(rfr);
		
		@SuppressWarnings("unused")
		String tempLine;
		int numberOfLines = 0;
		
		while ((tempLine = rbr.readLine()) != null) {
			numberOfLines++;
		}
		
		rbr.close();
		
		return numberOfLines;
	}
}
